#!/bin/sh
#
# test-string.sh: evaluates the value of a string
#
# Author: Bojan G. Kalicanin
# Git Version: $Id$

ANSWER=maybe

if [ -z "$ANSWER" ]; then
	echo "There is no answer." >&2
	exit 1
fi

if [ "$ANSWER" = "yes" ]; then
	echo "The answer is YES."
elif [ "$ANSWER" = "no" ]; then
	echo "The answer is NO."
elif [ "$ANSWER" = "maybe" ]; then
	echo "The answer is MAYBE."
else
	echo "The answer is UNKNOWN."
fi
