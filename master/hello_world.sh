#!/bin/sh
#
# helloworld.sh: Simple script that prints "Hello World"
#
# Author: Bojan G. Kalicanin
# Date:	30-Jun-2016
# Git $Id$

echo "Hello World!"
